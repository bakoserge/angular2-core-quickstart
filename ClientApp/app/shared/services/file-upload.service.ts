﻿import { Injectable } from '@angular/core';
//import { HttpRequest, HttpResponse, HttpEventType, HttpClient } from "@angular/commonhttp";
import { Headers, Http, Response, RequestOptions, Request } from '@angular/http';
import { Observable } from 'rxjs/Rx';


@Injectable()
export class FileUploadService {

    baseUrl = 'http://localhost:51092'; // our local Hapi Js API

    constructor(private _http: Http,
    //    private _httpClient: HttpClient
    ) { }

    upload(fileToUpload: any) {
        //let headers = new Headers({ 'Content-Type': 'multipart/form-data' });
        //let options = new RequestOptions({ headers: headers });

        console.log('here')
        let input = new FormData();
        input.append("file", fileToUpload);

        //let req = new HttpRequest('POST', '/api/upload/file', input, {
        //    reportProgress: true,
        //});

        //return this._httpClient.request(req)
        //    .subscribe(event => {
        //        // Via this API, you get access to the raw event stream.
        //        // Look for upload progress events.
        //        if (event.type === HttpEventType.UploadProgress) {
        //            // This is an upload progress event. Compute and show the % done:
        //            const percentDone = Math.round(100 * event.loaded / event.total);
        //            console.log(`File is ${percentDone}% uploaded.`);
        //        } else if (event instanceof HttpResponse) {
        //            console.log('File is completely uploaded!');
        //        }
        //    });

        return this._http.post("api/upload/file", input, {

        })
        .map(this.extractData)
        .catch(this._handlerError.bind(this));
    }

    _handlerError(err: any) {
        console.log(err);
        return Observable.throw(err);
    }
    private extractData(res: Response) {
        let body = res.json();
        return body.data || {};
    }
}