import { NgModule } from '@angular/core';
import { FormsModule, FormGroup, FormBuilder, FormControl, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { UniversalModule } from 'angular2-universal';
import { AppComponent } from './components/app/app.component'
import { HomeComponent } from './components/home/home.component';
import { WebService, FileUploadService, CustomBrowserXhr, ProgressService } from './services-barrel';
import { EllipsisPipe } from './shared/pipes/EllipsisPipe';
import { BrowserXhr } from "@angular/http";

@NgModule({
    
    providers: [
        ProgressService,
        { provide: BrowserXhr, useClass: CustomBrowserXhr, deps: [ProgressService] },
        WebService,
        FileUploadService,
        
    ],
    bootstrap: [AppComponent],
    declarations: [
        AppComponent,
        HomeComponent,
        EllipsisPipe,
    ],
    imports: [
        UniversalModule, // Must be first import. This automatically imports BrowserModule, HttpModule, and JsonpModule too.
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forRoot([
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', component: HomeComponent},
            { path: '**', redirectTo: 'home' }
        ])
    ]
})
export class AppModule {
}
